package com.kata.calcul_rpn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculRpnApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculRpnApplication.class, args);
	}

}
