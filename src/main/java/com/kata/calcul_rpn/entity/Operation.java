package com.kata.calcul_rpn.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Entity
public class Operation {
    @Id
    String id ;
    String operator ;
    List<Float> old_stack_values ;
    String stackId ;
    LocalDateTime createdDate ;

}
