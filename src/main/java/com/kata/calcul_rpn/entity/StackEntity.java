package com.kata.calcul_rpn.entity;

import jakarta.persistence.*;
import lombok.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class StackEntity {
    @Id
    String id ;
    List<Float> stack_values = new ArrayList<>();

}
