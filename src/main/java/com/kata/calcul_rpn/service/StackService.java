package com.kata.calcul_rpn.service;

import com.kata.calcul_rpn.entity.Operation;
import com.kata.calcul_rpn.entity.Operator;
import com.kata.calcul_rpn.entity.StackEntity;
import com.kata.calcul_rpn.repository.OperationRepository;
import com.kata.calcul_rpn.repository.StackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class StackService {
    private final StackRepository stackRepository;

    private final OperationRepository operationRepository;

    public StackEntity createStack() {

        return stackRepository.save(new StackEntity(UUID.randomUUID().toString(),new ArrayList<>()) );
    }

    public StackEntity getStack(String stackId) {
        StackEntity stack = stackRepository.findById(stackId).orElseThrow(() -> new NoSuchElementException("Stack not found"));
        return stack ;
    }

    public StackEntity pushValue(String stackId, float value) {
        StackEntity stack = stackRepository.findById(stackId).orElseThrow(() -> new NoSuchElementException("Stack not found"));
        stack.getStack_values().add(value);
        return stackRepository.save(stack);
    }

    public StackEntity applyOperand(String stackId, String operand) {
        StackEntity stack = stackRepository.findById(stackId).orElseThrow(() -> new NoSuchElementException("Stack not found"));
        List<Float> values = stack.getStack_values();
        List<Float> old_values = new ArrayList<>(stack.getStack_values()); //  copy of the old values
        Operation operation = Operation.builder()
                .id(UUID.randomUUID().toString())
                .stackId(stackId)
                .old_stack_values(old_values)
                .operator(null)
                .createdDate(LocalDateTime.now())
                .build();
        if (values.size() < 2) {
            throw new IllegalArgumentException("Not enough operands for the operand");
        }
        float num2 = values.remove(values.size() - 1);
        float num1 = values.remove(values.size() - 1);
        switch (operand) {
            case "add":
                values.add(num1 + num2);
                operation.setOperator(Operator.ADDITION.getSymbol());

                break;
            case "sous":
                values.add(num1 - num2);
                operation.setOperator(Operator.SUBTRACTION.getSymbol());
                break;
            case "mul":
                values.add(num1 * num2);
                operation.setOperator(Operator.MULTIPLICATION.getSymbol());
                break;
            case "div":
                if (num2 == 0) {
                    throw new IllegalArgumentException("Division by zero is not allowed");
                }
                operation.setOperator(Operator.DIVISION.getSymbol());
                values.add(num1 / num2);
                break;
            default:
                throw new IllegalArgumentException("Invalid operand: " + operand);
        }
        operationRepository.save(operation) ;
        return stackRepository.save(stack);
    }

    public StackEntity clearStack(String stackId) {
        StackEntity stack = stackRepository.findById(stackId).orElseThrow(() -> new NoSuchElementException("Stack not found"));
        stack.getStack_values().clear();
        return stackRepository.save(stack);
    }

    public List<StackEntity> getAllStacks() {
        return stackRepository.findAll();
    }

    public void deleteStack(String stackId) {
        stackRepository.deleteById(stackId);
    }
}
