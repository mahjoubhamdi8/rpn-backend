package com.kata.calcul_rpn.service;

import com.kata.calcul_rpn.entity.Operation;
import com.kata.calcul_rpn.repository.OperationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OperationService {
    private final OperationRepository operationRepository;

    public List<Operation> getAllOperations() {
        return operationRepository.findAll();
    }

    public List<Operation> getOperationsByStackId(String stackId) {
        return operationRepository.findByStackIdOrderByCreatedDate(stackId);
    }
}
