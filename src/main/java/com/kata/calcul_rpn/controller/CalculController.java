package com.kata.calcul_rpn.controller;

import com.kata.calcul_rpn.entity.StackEntity;
import com.kata.calcul_rpn.service.OperationService;
import com.kata.calcul_rpn.service.StackService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/rpn")
@RequiredArgsConstructor
public class CalculController {
    private final StackService stackService;
    private final OperationService operationService;

    @GetMapping(path = "/op" , produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            description = "list all the operand",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = com.kata.calcul_rpn.entity.Operation.class))
                            ))
            })
    public ResponseEntity<List<com.kata.calcul_rpn.entity.Operation>> getOperands() {
        return ResponseEntity.ok().body(operationService.getAllOperations());
    }
    @PostMapping(path = "/op/{op}/stack/{stack_id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            description = "Apply an operand to a stack",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = StackEntity.class)
                            )
                            )
            })
    public ResponseEntity<StackEntity> ApplyOperand (@PathVariable("op") String op , @PathVariable("stack_id") String stack_id )  {
        return ResponseEntity.ok().body(stackService.applyOperand(stack_id , op)) ;
    }

    @PostMapping(path = "/stack" , produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            description = "Create new stack",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = StackEntity.class)
                            ))
            })
    public ResponseEntity<StackEntity> createStack () {
        return ResponseEntity.ok().body(stackService.createStack()) ;
    }
    @GetMapping(path = "/stack" , produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            description = "List the available stacks",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StackEntity.class))                            ))
            })
    public ResponseEntity<List<StackEntity>> getListStacks () {
        return ResponseEntity.ok().body(stackService.getAllStacks()) ;
    }
    @DeleteMapping(path = "/stack/{stack_id}")
    @Operation(
            description = " delete a stack",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted")
            })
    public ResponseEntity<Void> deleteStack (@PathVariable ("stack_id") String stack_id) {
        stackService.deleteStack(stack_id);
        return ResponseEntity.ok().build() ;
    }

    @PostMapping(path = "/stack/{stack_id}")
    @Operation(
            description = " push new value to a stack ",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = StackEntity.class)
                            ))
            })
    public ResponseEntity<StackEntity> pushNewvalue (@PathVariable ("stack_id") String stack_id , @RequestParam("value") float value) {
        return ResponseEntity.ok().body(stackService.pushValue(stack_id , value));
    }

    @GetMapping(path = "/stack/{stack_id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            description = "get a stack",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = StackEntity.class)
                            ))
            })
    public ResponseEntity<StackEntity> getStack (@PathVariable ("stack_id") String stack_id) {

        return ResponseEntity.ok().body(stackService.getStack(stack_id));
    }

    @GetMapping(path = "/op/{stackId}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            description = "list all the operation done to a stack",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = com.kata.calcul_rpn.entity.Operation.class))
                            ))
            })
    public ResponseEntity<List<com.kata.calcul_rpn.entity.Operation>> getOperands(@PathVariable String stackId) {
        return ResponseEntity.ok().body(operationService.getOperationsByStackId(stackId));
    }

    @PutMapping(path = "/stack/{stack_id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            description = "clear (reset) a stack",
            responses = {
                    @ApiResponse(responseCode = "400", ref = "badRequestAPI"),
                    @ApiResponse(responseCode = "500", ref = "internalServerErrorAPI"),
                    @ApiResponse(responseCode = "200", description = "Request successfully excecuted",
                            content = @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = StackEntity.class)
                            ))
            })
    public ResponseEntity<StackEntity> clearStack (@PathVariable ("stack_id") String stack_id) {

        return ResponseEntity.ok().body(stackService.clearStack(stack_id));
    }


}
