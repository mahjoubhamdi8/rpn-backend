package com.kata.calcul_rpn.config;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.examples.Example;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.responses.ApiResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

@Configuration
@OpenAPIDefinition
public class OpenApiConfig {
    @Bean
    public OpenAPI baseOpenAPI() {
        ApiResponse badRequestAPI = new ApiResponse().content(
                new Content().addMediaType(MediaType.APPLICATION_JSON_VALUE ,
                        new io.swagger.v3.oas.models.media.MediaType().addExamples("default" ,
                                new Example().value("{\"code\" : 400 , \"Status\" : \"Bad Request!\" , \"Message\" :\"Bad Request!\"}")))
        ).description("Bad Request !");

        ApiResponse internalServerErrorAPI = new ApiResponse().content(
                new Content().addMediaType(MediaType.APPLICATION_JSON_VALUE ,
                        new io.swagger.v3.oas.models.media.MediaType().addExamples("default" ,
                                new Example().value("{\"code\" : 500 , \"Status\" : \"internal server error!\" , \"Message\" :\"internal server error!\"}")))
        ).description("internal server error!");

        Components components = new Components() ;
        components.addResponses("badRequestAPI" , badRequestAPI);
        components.addResponses("internalServerErrorAPI" , internalServerErrorAPI);
        return new OpenAPI().components(components).info(new Info().title("Spring Doc").version("1.0.0").description("Spring Doc")) ;
    }
}