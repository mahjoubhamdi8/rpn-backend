package com.kata.calcul_rpn.repository;

import  com.kata.calcul_rpn.entity.StackEntity ;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StackRepository extends JpaRepository<StackEntity, String> {

}