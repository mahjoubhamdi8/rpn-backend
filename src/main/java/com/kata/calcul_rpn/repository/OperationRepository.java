package com.kata.calcul_rpn.repository;

import com.kata.calcul_rpn.entity.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<Operation, String> {
    List<Operation> findByStackIdOrderByCreatedDate(String stackId) ;

}
