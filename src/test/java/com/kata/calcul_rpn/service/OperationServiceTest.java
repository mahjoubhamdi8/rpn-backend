package com.kata.calcul_rpn.service;

import com.kata.calcul_rpn.entity.Operation;
import com.kata.calcul_rpn.repository.OperationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OperationServiceTest {

    @InjectMocks
    OperationService operationService;
    @Mock
    OperationRepository operationRepository;
    @Test
    void shouldGetAllOperations() {
        Operation op1 = Operation.builder().id("id1").operator("+").stackId("id").old_stack_values(new ArrayList<>(Arrays.asList(1.0f, 2.0f, 3.0f, 4.0f, 5.0f))).build() ;
        Operation op2 = Operation.builder().id("id2").operator("+").stackId("id").old_stack_values(new ArrayList<>(Arrays.asList(1.0f, 2.0f, 3.0f, 4.0f, 5.0f))).build() ;
        List<Operation> operations = new ArrayList<>();
        when(operationRepository.findAll()).thenReturn(operations);
        List<Operation> result = operationService.getAllOperations();
        // Verify that the method was called once
        verify(operationRepository, times(1)).findAll();
        assertEquals(operations.size(), result.size());
        assertEquals(operations, result);
    }

    @Test
    public void testGetOperationsByStackIdInAscendingOrder() {
        // Create a test stackId
        String stackId = "testStackId";

        // Create a list of test operations
        List<Operation> testOperations = new ArrayList<>();
        testOperations.add(new Operation("3", "*", new ArrayList<>(), "testStackId", LocalDateTime.now().minusMinutes(20)));
        testOperations.add(new Operation("2", "-", new ArrayList<>(), "testStackId", LocalDateTime.now().minusMinutes(10)));
        testOperations.add(new Operation("1", "+", new ArrayList<>(), "testStackId", LocalDateTime.now()));

        when(operationRepository.findByStackIdOrderByCreatedDate(stackId)).thenReturn(testOperations);
        List<Operation> result = operationService.getOperationsByStackId(stackId);
        verify(operationRepository, times(1)).findByStackIdOrderByCreatedDate(stackId);
        assertNotNull(result);
        assertEquals(3, result.size());
        // Assert that operations are ordered by their createdDate in ascending order
        for (int i = 0; i < result.size() - 1; i++) {
            LocalDateTime current = result.get(i).getCreatedDate();
            LocalDateTime next = result.get(i + 1).getCreatedDate();
            assertTrue(current.isBefore(next));
        }
    }
}
