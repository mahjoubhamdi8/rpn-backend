package com.kata.calcul_rpn.service;

import com.kata.calcul_rpn.entity.Operation;
import com.kata.calcul_rpn.entity.StackEntity;
import com.kata.calcul_rpn.repository.OperationRepository;
import com.kata.calcul_rpn.repository.StackRepository;
import jakarta.websocket.ClientEndpointConfig;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.*;
import java.util.stream.Stream;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StackServiceTest {

    @InjectMocks
    StackService stackService;
    @Mock
    StackRepository stackRepository;
    @Mock
    OperationRepository operationRepository;

    @Test
    void shouldCreateStack() {
        // Given
        when(stackRepository.save(any(StackEntity.class)))
                .thenReturn(getTestStackwithoutValues());
        StackEntity actual= stackService.createStack() ;
        Assertions.assertThat(actual.getStack_values()).isEqualTo(new ArrayList<>());
    }

    @Test
    void shouldGetStackbyId() {
        when(stackRepository.findById(anyString())).thenReturn(Optional.ofNullable(getTestStack()));
        StackEntity expected =StackEntity.builder().id("id").stack_values(new ArrayList<>(Arrays.asList(1.0f, 2.0f, 3.0f, 4.0f, 5.0f))).build() ;
        Assertions.assertThat(stackService.getStack("id")).isEqualTo(expected);
    }
    private StackEntity getTestStack() {
        return StackEntity.builder().id("id").stack_values(new ArrayList<>(Arrays.asList(1.0f, 2.0f, 3.0f, 4.0f, 5.0f))).build() ;
    }
    private Operation getTestOperation() {
        return Operation.builder().id("id").operator("+").stackId("id").old_stack_values(new ArrayList<>(Arrays.asList(1.0f, 2.0f, 3.0f, 4.0f, 5.0f))).build() ;
    }
    private StackEntity getTestStackwithoutValues() {
        return StackEntity.builder().id("id").stack_values(new ArrayList<>()).build() ;
    }
 // Invalid Apply operation because number of values is less then  2
    public static Stream<StackEntity> produceInvalidApplyOpertion() {
        return Stream.of(
                StackEntity.builder()
                        .id("id")
                        .stack_values(new ArrayList<>(Arrays.asList(1.0f)))
                        .build(),
                StackEntity.builder()
                        .id("id")
                        .stack_values(new ArrayList<>())
                        .build() );
    }

    @ParameterizedTest
    @MethodSource("produceInvalidApplyOpertion")
    void shouldThrowExceptionWhenInvalidApplyOperation(StackEntity stack) {
        when(stackRepository.findById(anyString())).thenReturn(Optional.ofNullable(stack));

        assertThatThrownBy(() -> stackService.applyOperand(stack.getId() , "+"))
                .isInstanceOfAny(IllegalArgumentException.class) ;
    }
    @Test
    void shouldApplyOperation() {
        when(stackRepository.findById(anyString())).thenReturn(Optional.ofNullable(getTestStack()));
        when(stackRepository.save(any(StackEntity.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        when(operationRepository.save(any(Operation.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        StackEntity expected = StackEntity.builder().id("id").stack_values(new ArrayList<>(Arrays.asList(1.0f, 2.0f, 3.0f, 9.0f))).build() ;
        StackEntity actual = stackService.applyOperand("id" , "add") ;
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void testPushValue() {
        // Given
        String stackId = "stack1";
        float value = 10.0f;

        StackEntity stackEntity = new StackEntity();
        stackEntity.setId(stackId);
        List<Float> stackValues = new ArrayList<>();
        stackEntity.setStack_values(stackValues);
        when(stackRepository.findById(stackId)).thenReturn(Optional.of(stackEntity));
        when(stackRepository.save(any(StackEntity.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));

        // When
        StackEntity result = stackService.pushValue(stackId, value);

        // Then
        assertNotNull(result);
        assertEquals(1, result.getStack_values().size());
        assertEquals(value, result.getStack_values().get(0));
    }

    private void assertNotNull(StackEntity result) {
    }

    @Test
    public void testPushValueWhenStackNotFound() {
        // Given
        String stackId = "nonExistentStack";
        float value = 10.0f;

        // Mock the behavior of stackRepository.findById
        when(stackRepository.findById(stackId)).thenReturn(Optional.empty());

        // When and Then
        assertThrows(NoSuchElementException.class, () -> stackService.pushValue(stackId, value));
    }

    @Test
    public void testClearStack() {
        // Given
        String stackId = "stack1";

        // Create a sample StackEntity
        StackEntity stackEntity = new StackEntity();
        stackEntity.setId(stackId);
        List<Float> stackValues = new ArrayList<>();
        stackValues.add(10.0f);
        stackValues.add(20.0f);
        stackEntity.setStack_values(stackValues);

        // Mock the behavior of stackRepository.findById
        when(stackRepository.findById(stackId)).thenReturn(Optional.of(stackEntity));

        // When
        stackService.clearStack(stackId);

        // Then
        verify(stackRepository, times(1)).findById(stackId);
        verify(stackRepository, times(1)).save(stackEntity);

        // Verify that the stackValues list is cleared after calling the method
        assertEquals(0, stackEntity.getStack_values().size());
    }

    @Test
    public void testClearStackWhenStackNotFound() {
        // Given
        String stackId = "nonExistentStack";

        // Mock the behavior of stackRepository.findById
        when(stackRepository.findById(stackId)).thenReturn(Optional.empty());

        // When and Then
        assertThrows(NoSuchElementException.class, () -> stackService.clearStack(stackId));

        // Verify that the stackRepository.save method is not called when stack is not found
        verify(stackRepository, never()).save(any(StackEntity.class));
    }
    @Test
    public void testGetAllStacks() {
        // Given
        List<StackEntity> stacks = new ArrayList<>();
        StackEntity stack1 = StackEntity.builder()
                .id("stack1")
                .stack_values(Arrays.asList(10.0f, 20.0f))
                .build();
        stacks.add(stack1);

        StackEntity stack2 = StackEntity.builder()
                .id("stack2")
                .stack_values(Arrays.asList(10.0f, 20.0f))
                .build();
        stacks.add(stack2);

        // Mock the behavior of stackRepository.findAll
        when(stackRepository.findAll()).thenReturn(stacks);

        // When
        List<StackEntity> result = stackService.getAllStacks();

        // Then
        assertEquals(2, result.size());

        // Verify that the stackRepository.findAll method is called once
        verify(stackRepository, times(1)).findAll();
    }

    @Test
    public void testGetAllStacks_EmptyList() {
        // Given
        List<StackEntity> mockStacks = new ArrayList<>();

        // Mock the behavior of stackRepository.findAll
        when(stackRepository.findAll()).thenReturn(mockStacks);

        // When
        List<StackEntity> result = stackService.getAllStacks();

        // Then
        assertTrue(result.isEmpty());

        // Verify that the stackRepository.findAll method is called once
        verify(stackRepository, times(1)).findAll();
    }
}
