package com.kata.calcul_rpn.controller;

import com.kata.calcul_rpn.entity.StackEntity;
import com.kata.calcul_rpn.service.OperationService;
import com.kata.calcul_rpn.service.StackService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebFluxTest
public class CalculControllerTest {
    @MockBean
    private StackService stackService;
    @MockBean
    OperationService operationService;

    @Test
    public void testGetListStacks() {
        // Arrange
        List<StackEntity> stacks = Arrays.asList(
                new StackEntity("Stack 1" , new ArrayList<>(Arrays.asList(1.0f))),
                new StackEntity("Stack 2" , new ArrayList<>(Arrays.asList(2.0f)))
        );

        Mockito.when(stackService.getAllStacks()).thenReturn(stacks);

        WebTestClient client = WebTestClient.bindToController(new CalculController(stackService , operationService)).build();

        // Act & Assert
        client.get()
                .uri("/rpn/stack")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(StackEntity.class)
                .isEqualTo(stacks);
    }
}
