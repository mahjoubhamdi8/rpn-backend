# RPN Kata BACKEND
Ce projet a été développé en spring boot (java 17). ce projet expose un API REST d'une calculatrice RPN avec des méthodes des tests unitaires avec JUnit.

## Dockerisation:
le Build d'un image docker :

>  mvn spring-boot:build-image 

le Run d'un docker container :

> docker run -p 8080:8080 docker.io/hamdi888/rpn-calcul_rpn:0.0.1-SNAPSHOT

Docker Compose: 

> docker-compose up